2019-01-07新增拼音码生成器
2018-12-18修复查询
2018-12-06修复了大部分bug与添加了很多报表可以用在小型企业店铺使用数据库文件在data文件夹里
![输入图片说明](https://images.gitee.com/uploads/images/2018/1206/103000_21db8d38_1320391.png "123.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1212/111644_91b0274d_1320391.png "QQ图片20181212111213.png")
![输入图片说明](https://images.gitee.com/uploads/images/2018/1212/111654_84b2948b_1320391.png "QQ图片20181212111306.png")
2018-06-05修复jqgrid总行数不正确，增加了导出功能 :smile: 
登录用cookie记住账号密码
![输入图片说明](https://gitee.com/uploads/images/2018/0521/151526_fba01e73_1320391.png "core5.png")
用的是.net core+EF+codeFirst+mysql使用layui组件+jqgrid表格
![输入图片说明](https://gitee.com/uploads/images/2018/0521/150023_d774be67_1320391.png "core1.png")
权限控制到按钮
![输入图片说明](https://gitee.com/uploads/images/2018/0521/150035_2e63ab03_1320391.png "core2.png")

![输入图片说明](https://gitee.com/uploads/images/2018/0521/150043_d02004cf_1320391.png "core3.png")
支持多级菜单
![输入图片说明](https://images.gitee.com/uploads/images/2018/1212/111616_11b08807_1320391.png "QQ图片20181212111306.png")
数据库生成迁移参考https://docs.microsoft.com/zh-cn/aspnet/core/data/ef-mvc/migrations?view=aspnetcore-2.0
![输入图片说明](https://gitee.com/uploads/images/2018/0522/083812_4ddede65_1320391.png "core6.png")




